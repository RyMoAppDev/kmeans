#!/bin/bash

sudo rm -rf build
mkdir build
cd build
cmake -DBUILD_TESTING=ON -DCMAKE_BUILD_TYPE=Debug ..
make -j4
ctest --verbose
