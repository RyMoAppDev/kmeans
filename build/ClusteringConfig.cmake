set(Clustering_VERSION 0.0.0.0)


####### Expanded from @PACKAGE_INIT@ by configure_package_config_file() #######
####### Any changes to this file will be overwritten by the next CMake run ####
####### The input file was ClusteringConfig.cmake.in                            ########

get_filename_component(PACKAGE_PREFIX_DIR "${CMAKE_CURRENT_LIST_DIR}/" ABSOLUTE)

macro(set_and_check _var _file)
  set(${_var} "${_file}")
  if(NOT EXISTS "${_file}")
    message(FATAL_ERROR "File or directory ${_file} referenced by variable ${_var} does not exist !")
  endif()
endmacro()

####################################################################################



if(NOT TARGET Clustering::Clustering)
  include("${CMAKE_CURRENT_LIST_DIR}/ClusteringTargets.cmake")
endif()

# Compatibility
get_property(Clustering_Clustering_INCLUDE_DIR TARGET Clustering::Clustering PROPERTY INTERFACE_INCLUDE_DIRECTORIES)

set(Clustering_LIBRARIES Clustering::Clustering)
set(Clustering_INCLUDE_DIRS "${Clustering_Clustering_INCLUDE_DIR}")
list(REMOVE_DUPLICATES Clustering_INCLUDE_DIRS)


