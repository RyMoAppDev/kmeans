set(KMeans_VERSION 0.0.0.0)


####### Expanded from @PACKAGE_INIT@ by configure_package_config_file() #######
####### Any changes to this file will be overwritten by the next CMake run ####
####### The input file was KMeansConfig.cmake.in                            ########

get_filename_component(PACKAGE_PREFIX_DIR "${CMAKE_CURRENT_LIST_DIR}/" ABSOLUTE)

macro(set_and_check _var _file)
  set(${_var} "${_file}")
  if(NOT EXISTS "${_file}")
    message(FATAL_ERROR "File or directory ${_file} referenced by variable ${_var} does not exist !")
  endif()
endmacro()

####################################################################################



if(NOT TARGET KMeans::KMeans)
  include("${CMAKE_CURRENT_LIST_DIR}/KMeansTargets.cmake")
endif()

# Compatibility
get_property(KMeans_KMeans_INCLUDE_DIR TARGET KMeans::KMeans PROPERTY INTERFACE_INCLUDE_DIRECTORIES)

set(KMeans_LIBRARIES KMeans::KMeans)
set(KMeans_INCLUDE_DIRS "${KMeans_KMeans_INCLUDE_DIR}")
list(REMOVE_DUPLICATES KMeans_INCLUDE_DIRS)


