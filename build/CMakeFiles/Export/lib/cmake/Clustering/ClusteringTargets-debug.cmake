#----------------------------------------------------------------
# Generated CMake target import file for configuration "Debug".
#----------------------------------------------------------------

# Commands may need to know the format version.
set(CMAKE_IMPORT_FILE_VERSION 1)

# Import target "Clustering::Clustering" for configuration "Debug"
set_property(TARGET Clustering::Clustering APPEND PROPERTY IMPORTED_CONFIGURATIONS DEBUG)
set_target_properties(Clustering::Clustering PROPERTIES
  IMPORTED_LOCATION_DEBUG "${_IMPORT_PREFIX}/lib/libClustering.0.0.0.0.dylib"
  IMPORTED_SONAME_DEBUG "@rpath/libClustering.0.0.0.0.dylib"
  )

list(APPEND _IMPORT_CHECK_TARGETS Clustering::Clustering )
list(APPEND _IMPORT_CHECK_FILES_FOR_Clustering::Clustering "${_IMPORT_PREFIX}/lib/libClustering.0.0.0.0.dylib" )

# Commands beyond this point should not need to know the version.
set(CMAKE_IMPORT_FILE_VERSION)
