
## Summary

This project will build a header only shared library that can be used for matrix multiplication and matrix transpose. A few of the major design decisions for the library are discussed below.
</br></br>
The matrix class uses templates to allow for generic type usage. The other benefit of using templates is that different matrix configurations become separate types. This allows for compile time checks which can help the user catch bugs faster.

</br>
The underlying datastructure used to store the matrix data is the standard library's vector.  Using this datastructure means that we have heap allocated memory that we don't have to manage.  This means the user can allocate large matrices without fear of a stack overflow.  The con to using vector is that for small matrices we are missing out on the speed of the stack.  A possible future improvement would be to create dynamic and static matrix classes that inherit from a base matrix class.  The dynamic class would use vector to store data and the static matrix class would use an array.  To squeeze even more performance out, the CRTP could be used to reduce the overhead of virtual functions.  This could make a significant difference if new matrices are being creating at a high rate (e.g. in a ros node spinning at a high hz).

## To Build

You can build natively on your system using cmake or via docker.</br>
</br>
To build natively with cmake:

```console
foo@bar:~$ cd Brain && mkdir build && cmake .. && make -j4
```

To build natively with cmake AND run tests:

```console
foo@bar:~$ cd Brain && mkdir build && cmake -DBUILD_TESTING=ON .. && make -j4
```

To build via docker and run tests:

```console
foo@bar:~$ cd Brain && docker build .
```

To build documentation:

```console
foo@bar:~$ cd Brain/doc && doxygen Doxygen.in
foo@bar:~$ open ./html/index.html
```

## Example Usage

Tranpose a matrix

```cpp
#include <Brain/Matrix.h>

using Matrix3f = Brain::Matrix<float, 3, 3>;

Matrix3f mat1;
mat1[0][0] = 1;
mat1[0][1] = 2;
mat1[0][2] = 3;
Matrix3f mat1T = mat1.transpose()
```

Matrix Multiplication

```cpp
#include <Brain/Matrix.h>

using Matrix53f = Brain::Matrix<int, 5, 3>;
using Matrix35f = Brain::Matrix<int, 3, 5>;

Matrix53f mat1(std::vector<int>(15, 5));
Matrix35f mat2(std::vector<std::vector<int>(3, std::vector<int>(5, 10)));

Brain::Matrix<int, 5, 5> result = mat1 * mat2;
result.print();
```

For more examples, please check out the tests folder.
