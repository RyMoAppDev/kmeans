#include <Clustering/KMeans.h>

#include <cstdlib>
#include <cmath>
#include <iostream>
#include <string>
#include <vector>

int main()
{
    int exitCode = EXIT_SUCCESS;

    constexpr auto k = 3;
    const auto iterations = 300;
    const auto number_of_runs = 10;
    constexpr auto features = 2;
    constexpr auto samples = 1000;

    using Feature = enoki::Array<double, 9>;
    using Dataset = enoki::Array<Feature, 2>;

    Dataset data(
        Feature(1, 1, 2, 8, 8, 9, 15, 15, 16),
        Feature(1, 2, 1, 8, 9, 8, 16, 15, 15));

    double total_elapsed = 0;
    Clustering::KMeans<double, features, samples, k> k_means(iterations);

    return exitCode;
}
