# Get the base Ubuntu image from Docker Hub
FROM ubuntu:latest

# Install necessary packages to build project
RUN apt-get update && \
    apt-get install -y build-essential git cmake autoconf libtool pkg-config doxygen

# Build Enoki
RUN git clone --recursive https://github.com/mitsuba-renderer/enoki \
    && cd enoki \


# Copy over source and set working directory
COPY . /usr/src/KMeans
WORKDIR /usr/src/KMeans

# Build library and tests
RUN rm -rf build \
    && mkdir -p build \
    && cd build \
    && rm -rf * \
    && cmake -DBUILD_TESTING=ON .. \
    && make -j4

# Run the test cases
RUN cd build && ctest
