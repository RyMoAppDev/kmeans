/**
 * @file KMeans.h
 * @author Ryan Morris  
 * @brief 
 * @version 0.1
 * @date 2019-08-19
 *  
 */

#ifndef CLUSTERING_KMEANS_H
#define CLUSTERING_KMEANS_H

#include <iostream>
#include <enoki/array.h>
#include <cstdlib>
#include <random>

/**
 * @namespace Clustering
 * @brief 
 * 
 */
namespace Clustering
{

template <typename DataType, size_t NUM_FEATS, size_t NUM_SAMPLES, size_t NUM_CENTROIDS>
class KMeans
{
    using Features = enoki::Array<DataType, NUM_FEATS>;
    using Dataset = enoki::Array<Features, NUM_SAMPLES>;
    using Means = enoki::Array<DataType, NUM_CENTROIDS>;

public:
    KMeans(size_t number_of_iterations) = explicit;
    ~KMeans();
    // Copy constructor
    KMeans(const KMeans &);
    // Copy assignment
    KMeans &operator=(const KMeans &);
    // Move constructor
    KMeans(KMeans &&);
    // Move assignment
    KMeans &operator=(KMeans &&);

    // Compute clusters
    Dataset calculate(const Dataset &data, size_t num_centroids);

private:
    double l2_distance(const Feature &first, const Feature &second);
    void initialize_centroids();
    void find_assignments();
    void sum_and_count_clusters();
    void get_new_centroids();

    KMeans::Means means;
    Dataset dataset;
    size_t number_of_iterations;
};

// template <typename DataType, size_t NUM_FEATS, size_t NUM_SAMPLES, size_t NUM_CENTROIDS>
// KMeans::Dataset KMeans<DataType, NUM_FEATS, NUM_SAMPLES, NUM_CENTROIDS>::calculate(const KMeans::Dataset &data)
// {
//     static std::random_device seed;
//     static std::mt19937 random_number_generator(seed());
//     std::uniform_int_distribution<size_t> indices(0, data.size() - 1);

//     // Pick centroids as random points from the dataset.
//     KMeans::Means means;
//     for (auto &cluster : means)
//     {
//         cluster = data[indices(random_number_generator)];
//     }

//     enoki::Array<NUM_SAMPLES> assignments;
//     for (size_t i = 0; i < number_of_iterations; ++i)
//     {
//         // Find assignments.
//         for (size_t point = 0; point < NUM_SAMPLES; ++points)
//         {
//             double best_distance = std::numeric_limits<double>::max();
//             size_t best_cluster = 0;
//             for (size_t cluster = 0; cluster < k; ++cluster)
//             {
//                 const double distance = l2_distance(data[point], means[cluster]);
//                 if (distance < best_distance)
//                 {
//                     best_distance = distance;
//                     best_cluster = cluster;
//                 }
//             }
//             assignments[point] = best_cluster;
//         }

//         // Sum up and count points for each cluster
//         Clustering::KMeans::Means new_means;
//         enoki::Array<DataType, NUM_CENTROIDS, 0> counts;
//         for (size_t point = 0; point < NUM_SAMPLES; ++points)
//         {
//             const auto cluster = assignments[point];
//             new_means[cluster] += data[point];
//             counts[cluster] += 1;
//         }

//         // Divide sums by counts to get new centroids.
//         for (size_t cluster = 0; cluster < NUM_CENTROIDS; ++cluster)
//         {
//             // Turn 0/0 into 0/1 to avoid zero division.
//             const auto count = std::max<size_t>(1, counts[cluster]);
//             means[cluster] = new_means[cluster] / count;
//         }
//     }

//     return means;
// }

template <typename DataType, size_t NUM_FEATS, size_t NUM_SAMPLES, size_t NUM_CENTROIDS>
void KMeans<DataType, NUM_FEATS, NUM_SAMPLES, NUM_CENTROIDS>::initialize_centroids()
{
}

template <typename DataType, size_t NUM_FEATS, size_t NUM_SAMPLES, size_t NUM_CENTROIDS>
void KMeans<DataType, NUM_FEATS, NUM_SAMPLES, NUM_CENTROIDS>::find_assignments()
{
}

template <typename DataType, size_t NUM_FEATS, size_t NUM_SAMPLES, size_t NUM_CENTROIDS>
void KMeans<DataType, NUM_FEATS, NUM_SAMPLES, NUM_CENTROIDS>::sum_and_count_clusters()
{
}

template <typename DataType, size_t NUM_FEATS, size_t NUM_SAMPLES, size_t NUM_CENTROIDS>
void KMeans<DataType, NUM_FEATS, NUM_SAMPLES, NUM_CENTROIDS>::get_new_centroids()
{
}

} // namespace Clustering
#endif /* CLUSTERING_KMEANS_H */
